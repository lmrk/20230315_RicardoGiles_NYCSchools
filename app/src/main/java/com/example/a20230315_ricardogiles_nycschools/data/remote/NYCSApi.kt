package com.example.a20230315_ricardogiles_nycschools.data.remote

import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.data.model.score.ScoreModelItemModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NYCSApi {

    @GET(ENDPOINT_NYCS)
    suspend fun getNYSC(): Response<List<NYCSModelItem>>

    @GET(ENDPOINT_SCORE)
    suspend fun getScore(
        @Query(DBN) dbn: String
    ): Response<List<ScoreModelItemModel>>

    companion object{
        //https://data.cityofnewyork.us/resource/s3k6-pzi2.json
        //https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=01M292

        const val BASE_URL = "https://data.cityofnewyork.us/"
        const val ENDPOINT_NYCS = "resource/s3k6-pzi2.json"
        const val ENDPOINT_SCORE = "resource/f9bf-2cp4.json"
        const val DBN = "dbn"
    }
}