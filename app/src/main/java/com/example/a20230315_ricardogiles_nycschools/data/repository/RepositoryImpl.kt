package com.example.a20230315_ricardogiles_nycschools.data.repository

import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.data.model.score.ScoreModelItemModel
import com.example.a20230315_ricardogiles_nycschools.data.remote.NYCSApi
import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val apiDetails: NYCSApi
) : Repository {
    override fun getNYCS(): Flow<ResponseType<List<NYCSModelItem>>> = flow{
        emit(ResponseType.LOADING)
        try {
            val response = apiDetails.getNYSC()
            if (response.isSuccessful){
                response.body()?.let { it ->
                    emit(ResponseType.SUCCESS(it))
                }
            } else{
                emit(ResponseType.ERROR(response.message()))
            }
        } catch (e:java.lang.Exception){
            emit(ResponseType.ERROR(e.localizedMessage as String))
        }
    }

    override fun getScore(dbn: String): Flow<ResponseType<List<ScoreModelItemModel>>> = flow{
        emit(ResponseType.LOADING)

        try {
            val response = apiDetails.getScore(dbn)
            if (response.isSuccessful){
                response.body()?.let { it ->
                    emit(ResponseType.SUCCESS(it))
                }
            } else{
                emit(ResponseType.ERROR(response.message()))
            }
        } catch (e:java.lang.Exception){
            emit(ResponseType.ERROR(e.localizedMessage as String))
        }
    }

}