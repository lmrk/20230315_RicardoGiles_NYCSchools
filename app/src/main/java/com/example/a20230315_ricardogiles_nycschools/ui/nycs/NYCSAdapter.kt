package com.example.a20230315_ricardogiles_nycschools.ui.nycs

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.databinding.ItemNycsBinding

class NYCSAdapter (
    private val nycsList: MutableList<NYCSModelItem> = mutableListOf(),
    private val clickListener: (NYCSModelItem) -> Unit
) : RecyclerView.Adapter<NYCSAdapter.ViewHolder>() {

    fun updateNYSCAdapter(newSeason: List<NYCSModelItem>) {
        nycsList.clear()
        nycsList.addAll(newSeason)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view: ItemNycsBinding) : RecyclerView.ViewHolder(view.root) {
        fun setup(nycsModel: NYCSModelItem, clickListener: (NYCSModelItem) -> Unit) {
            view.ivName.text = nycsModel.schoolName
            view.ivLocation.text = nycsModel.primaryAddressLine1 + ", " + nycsModel.city + ", " + nycsModel.stateCode
            itemView.setOnClickListener { clickListener(nycsModel) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemNycsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount(): Int = nycsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setup(nycsList[position], clickListener)
    }
}