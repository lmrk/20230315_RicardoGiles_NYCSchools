package com.example.a20230315_ricardogiles_nycschools.data.model.nycschools


import com.google.gson.annotations.SerializedName

data class NYCSModelItem(
    @SerializedName("dbn")
    val dbn: String = "",
    @SerializedName("school_name")
    val schoolName: String = "",
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String = "",
    @SerializedName("city")
    val city: String = "",
    @SerializedName("state_code")
    val stateCode: String = "",
    @SerializedName("phone_number")
    val phoneNumber: String = "",
    @SerializedName("school_email")
    val schoolEmail: String = "",
    @SerializedName("website")
    val website: String = "",
    @SerializedName("zip")
    val zip: String = "",
    @SerializedName("latitude")
    val latitude: String = "",
    @SerializedName("longitude")
    val longitude: String = ""
)