package com.example.a20230315_ricardogiles_nycschools.ui.score

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.example.a20230315_ricardogiles_nycschools.data.model.score.ScoreModelItemModel
import com.example.a20230315_ricardogiles_nycschools.databinding.FragmentScoreBinding
import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import com.example.a20230315_ricardogiles_nycschools.viewmodel.NYCSViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class ScoreFragment : BottomSheetDialogFragment() {
    private var _binding: FragmentScoreBinding? = null
    private val binding get() = _binding
    private val viewModel: NYCSViewModel by activityViewModels()

    companion object {
        const val TAG = "TeamDetailFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentScoreBinding.inflate(inflater, container, false)

        _binding!!.sClose.setOnClickListener {
            this.dismiss()
        }

        viewModel.scoreResult.observe(viewLifecycleOwner) {
            when (it) {
                is ResponseType.LOADING -> {
                }
                is ResponseType.SUCCESS -> {
                    initViews(it.response)
                }
                is ResponseType.ERROR -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }
        viewModel.getScores()

        return binding!!.root
    }

    private fun initViews(data: List<ScoreModelItemModel>){
        val web = viewModel.NYCObject.website
        val phone = viewModel.NYCObject.phoneNumber
        if(data.isNotEmpty()){
            with(_binding!!){
                sScore.text = "SCORE"
                sMath.text = "MATH: " + data[0].satMathAvgScore
                sReading.text = "READING: " + data[0].satCriticalReadingAvgScore
                sWriting.text = "WRITING: " + data[0].satWritingAvgScore
                sMath.isVisible = true
                sReading.isVisible = true
                sWriting.isVisible = true
            }
        }
        else {
            with(_binding!!){
                sScore.text = "NO SCORE FOUND"
                sMath.isVisible = false
                sReading.isVisible = false
                sWriting.isVisible = false
            }
        }
        with(_binding!!){
            sName.text = viewModel.NYCObject.schoolName
            sAdress.text = "Address: "+viewModel.NYCObject.primaryAddressLine1 + ", " + viewModel.NYCObject.city + ", " + viewModel.NYCObject.stateCode
            sPhone.text = "Phone: $phone"
            sWeb.text = "Web: $web"
            sPhone.setOnClickListener{
                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:$phone")
                startActivity(dialIntent)
            }
            sAdress.setOnClickListener{
                var latitude = viewModel.NYCObject.latitude
                var longitude = viewModel.NYCObject.longitude
                val gmmIntentUri =
                    Uri.parse("google.navigation:q=$latitude,$longitude")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }
            sWeb.setOnClickListener{
                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                openURL.data = Uri.parse("https://$web")
                startActivity(openURL)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        viewModel.schoolSel = false
    }
}