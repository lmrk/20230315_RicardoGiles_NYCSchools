package com.example.a20230315_ricardogiles_nycschools.ui.nycs

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.databinding.FragmentNycsBinding
import com.example.a20230315_ricardogiles_nycschools.ui.score.ScoreFragment
import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import com.example.a20230315_ricardogiles_nycschools.viewmodel.NYCSViewModel

class NYCSFragment : Fragment() {
    private var _binding: FragmentNycsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: NYCSViewModel by activityViewModels()

    private val mAdapter by lazy {
        NYCSAdapter {
            viewModel.NYCObject = it
            if (!viewModel.schoolSel){
                val modalBottomSheet = ScoreFragment()
                modalBottomSheet.show(childFragmentManager, ScoreFragment.TAG)
                viewModel.schoolSel = true
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentNycsBinding.inflate(inflater, container, false)

        binding.rvSchools.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        viewModel.nycsResult.observe(viewLifecycleOwner) {
            when (it) {
                is ResponseType.LOADING -> {
                }
                is ResponseType.SUCCESS-> {
                    initViews(it.response)
                }
                is ResponseType.ERROR -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }
        viewModel.getSchools()

        return binding.root
    }

    private fun initViews(data: List<NYCSModelItem>){
        data.let {
            mAdapter.updateNYSCAdapter(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}