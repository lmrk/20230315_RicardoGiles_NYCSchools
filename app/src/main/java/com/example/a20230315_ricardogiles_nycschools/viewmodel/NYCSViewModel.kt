package com.example.a20230315_ricardogiles_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.data.model.score.ScoreModelItemModel
import com.example.a20230315_ricardogiles_nycschools.data.repository.Repository
import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NYCSViewModel @Inject constructor(
    private val repository: Repository
): ViewModel(){

    lateinit var NYCObject: NYCSModelItem
    var schoolSel: Boolean = false

    private val _nycsResult = MutableLiveData<ResponseType<List<NYCSModelItem>>>()
    val nycsResult: LiveData<ResponseType<List<NYCSModelItem>>> = _nycsResult

    private val _scoreResult = MutableLiveData<ResponseType<List<ScoreModelItemModel>>>()
    val scoreResult: LiveData<ResponseType<List<ScoreModelItemModel>>> = _scoreResult

    fun getSchools(){
        viewModelScope.launch {
            repository.getNYCS().collect{
                _nycsResult.postValue(it)
            }
        }
    }

    fun getScores(dbn: String = NYCObject.dbn){
        viewModelScope.launch {
            repository.getScore(dbn).collect{
                _scoreResult.postValue(it)
            }
        }
    }
}