package com.example.a20230315_ricardogiles_nycschools.data.repository


import com.example.a20230315_ricardogiles_nycschools.util.ResponseType
import com.example.a20230315_ricardogiles_nycschools.data.model.nycschools.NYCSModelItem
import com.example.a20230315_ricardogiles_nycschools.data.model.score.ScoreModelItemModel
import kotlinx.coroutines.flow.Flow

interface Repository {
    fun getNYCS(): Flow<ResponseType<List<NYCSModelItem>>>
    fun getScore(dbn: String): Flow<ResponseType<List<ScoreModelItemModel>>>
}